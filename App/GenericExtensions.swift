//
//  Extensions.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 12/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import UIKit

@IBDesignable
//This configure every button to have the same radius rounded corner control
class CustomButton: UIButton {
    @IBInspectable var cornerRadius: CGFloat = 6 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
            self.layer.masksToBounds = true
        }
    }
}


//This extends the resign first responder (dismiss keyboard) for any textfield that have return key.
extension BaseViewController : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) {
            nextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            return true
        }
        return false
    }
}
