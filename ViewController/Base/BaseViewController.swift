//
//  BaseViewController.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 11/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import UIKit

open class BaseViewController : UIViewController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    open override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    internal func showAlert(title: String, message : String, buttonText : String) {
        let alert = UIAlertController.init(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: buttonText, style: .default) { (action) in }
        alert.addAction(okAction)
        self.present(alert,animated: true, completion: nil)
    }
    
    //Need to fix this asap to remove Objective C dependency for endEditing.
    @objc func handleTap() {
        view.endEditing(true)
    }
}
