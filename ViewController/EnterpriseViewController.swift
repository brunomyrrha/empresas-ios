//
//  EnterpriseViewController.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 11/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//


//BOSS
import UIKit

class EnterpriseViewController : BaseViewController {
    
    @IBOutlet weak var logoImageView : UIImageView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var topBar : UIImageView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var searchBar : UISearchBar!
    
    var logoPosition : CGAffineTransform?
    var searchPosition : CGAffineTransform?
    var cancelPosition : CGAffineTransform?
    var searchFieldPosition : CGAffineTransform?
    let screenSize = UIScreen.main.bounds
    let presenter = EnterprisePresenter()
    let searchBarController = UISearchController(searchResultsController: nil)
    var isFilterOn : Bool = false
    var enterpriseList = [Enterprise]()
    var filteredEnterpriseList = [Enterprise]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(self)
        
        //Initial setup methods
        configureBehavior()
        loadEnterpriseList()
        
        //Animation related stuff
        recordOriginalPositions()
        defineAnimationPositions()
    }
    
    @IBAction func cancelTap(_ sender: Any) {
        animate(asIntro: false)
    }
    
    @IBAction func searchTap(_ sender: Any) {
        animate(asIntro: true)
    }
    
    private func animate(asIntro : Bool) {
        if asIntro{
            UIView.animate(withDuration: 0.5, animations: {
                self.searchButton.transform = CGAffineTransform(translationX: self.screenSize.maxX, y:0)
                self.logoImageView.transform = CGAffineTransform(translationX: 0, y: -self.screenSize.maxY)
                self.cancelButton.transform = self.cancelPosition!
                self.searchBar.transform = self.searchFieldPosition!
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.searchButton.transform = self.searchPosition!
                self.logoImageView.transform = self.logoPosition!
                self.cancelButton.transform = CGAffineTransform(translationX: self.screenSize.maxX, y: 0)
                self.searchBar.transform = CGAffineTransform(translationX: -self.screenSize.maxX, y: 0)
            })
        }
    }
    
    //This set this class as the delegate for textField and table view.
    private func configureBehavior() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.searchBar.backgroundImage = UIImage() //This removes 1 pixel height margin from searchBar
        
        //Search Bar configure and control
        searchBar.delegate = self
        searchBar.returnKeyType = UIReturnKeyType.done
    }
    
    //This stores all final position for animation
    private func recordOriginalPositions(){
        logoPosition = logoImageView.transform
        searchPosition = logoImageView.transform
        cancelPosition = cancelButton.transform
        searchFieldPosition = searchBar.transform
    }
    
    //This removes the elements that aren't supposed to be shown on first use
    private func defineAnimationPositions() {
        cancelButton.transform = CGAffineTransform(translationX: screenSize.maxX, y: 0)
        searchBar.transform = CGAffineTransform(translationX: -screenSize.maxX, y: 0)
    }
    
    private func loadEnterpriseList(){
        self.presenter.getEnterprises()
    }

    private func filterCellWith(_ searchText : String) {
        if (!searchText.isEmpty){
            filteredEnterpriseList = enterpriseList.filter({
                (enterprise) -> Bool in
                enterprise.enterpriseName?.contains(searchText) ?? false
            })
        } else {
            filteredEnterpriseList = enterpriseList
        }
        tableView.reloadData()
    }
}

extension EnterpriseViewController : EnterpriseProtocol{
    
    func showAlert(title: String, message: String) {
        super.showAlert(title: title, message: message, buttonText: "OK")
    }
    
    func populateList(_ enterpriseList: [Enterprise]) {
        self.enterpriseList.append(contentsOf: enterpriseList)
        tableView.reloadData()
    }
}

extension EnterpriseViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFilterOn{
            return filteredEnterpriseList.count
        }
        return enterpriseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! EnterpriseCell
        let enterprise : Enterprise
        if isFilterOn{
            enterprise = filteredEnterpriseList[indexPath.row]
        } else {
            enterprise = enterpriseList[indexPath.row]
        }
        cell.enterprise = enterprise
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let enterprise : Enterprise = enterpriseList[indexPath.row]
        let enterpriseDetailsViewController = storyboard!.instantiateViewController(withIdentifier: "Enterprise Details View Controller") as! EnterpriseDetailsViewController
        enterpriseDetailsViewController.enterpriseName = enterprise.enterpriseName!
        enterpriseDetailsViewController.enterpriseDescription = enterprise.description!
        self.present(enterpriseDetailsViewController, animated: true, completion: nil)
    }
}

extension EnterpriseViewController : UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        isFilterOn = true
        filterCellWith(searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isFilterOn = false
        animate(asIntro: false)
        self.searchBar.endEditing(true)
        self.searchBar.resignFirstResponder()
    }
}
