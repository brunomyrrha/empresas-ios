//
//  EnterpriseDetailViewController.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 16/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//


//INTERN
import UIKit

class EnterpriseDetailsViewController : BaseViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textLabel: UITextView!
    
    var enterpriseName = ""
    var enterpriseDescription = ""
    
    override func viewDidLoad() {
        self.nameLabel.text = enterpriseName
        self.textLabel.text = enterpriseDescription
    }
}
