//
//  ViewController.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 05/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import UIKit

class LoginViewController : BaseViewController  {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    var presenter : LoginPresenter = LoginPresenter()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.presenter.attachView(self)
        configureBehavior()
        debug(true)
    }
    
    private func debug(_ enabled : Bool) {
    if (enabled) {
            presenter.makeLogin(login: "testeapple@ioasys.com.br", password: "12341234")
        }
    }
    
    private func configureBehavior() {
        emailTextField.delegate = self
        passwordTextField.delegate = self
        dismissWithTapGesture()
    }
    
    @IBAction func onLoginClick(_ sender: Any) {
        if (emailTextField.text!.isEmpty || passwordTextField.text!.isEmpty){
            super.showAlert(title: "Campos necessários", message: "Preencha todos os campos antes de fazer o login.", buttonText: "OK")
        } else {
            presenter.makeLogin(login: emailTextField.text!, password: passwordTextField.text!)
        }
    }
    
    //This sets the UI Text Field to resign the first responder when somewhere else is tapped. This was moved here to make the tap gesture on table view works.
    private func dismissWithTapGesture() {
        let tapScreen = UITapGestureRecognizer(target: self, action: #selector(BaseViewController.handleTap))
        view.addGestureRecognizer(tapScreen)
    }
}

extension LoginViewController : LoginProtocol {
    func loginDone(user : User) {
        //Store User somewhere in memory (The API already does that).
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let enterpriseViewController = storyBoard.instantiateViewController(withIdentifier: "Enterprise View Controller") as! EnterpriseViewController
        self.present(enterpriseViewController, animated: true, completion: nil)
    }
    
    func showAlert(title: String, message: String) {
        super.showAlert(title: title, message: message, buttonText: "OK")
    }
}
