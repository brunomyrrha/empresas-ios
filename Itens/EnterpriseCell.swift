//
//  EnterpriseDataTableViewCell.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 13/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import UIKit

class EnterpriseCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    
    var enterprise: Enterprise? = nil {
        didSet {
            nameLabel.text = enterprise?.enterpriseName
            typeLabel.text = enterprise?.entepriseType?.enterpriseTypeName
            countryLabel.text = enterprise?.country
        }
    }
}
