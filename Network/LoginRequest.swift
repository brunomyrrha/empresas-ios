//
//  LoginRequest.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 15/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation


class LoginRequest :APIRequest {
    
    @discardableResult
    static func makeLogin(username: String,
                          password: String,
                          onResponse: ((User)->Void)?,
                          onError: ((API.RequestError)->Void)?) -> LoginRequest {
        
        let myRequest = LoginRequest(
            method: .post,
            path: "users/auth/sign_in",
            parameters: ["email":username,"password":password],
            urlParameters: nil,
            cacheOption: .networkOnly)
        {
            (response, error, cache) in
            if let error = error {
                onError?(error)
            } else if let response = response as? [String : Any] {
                let user = User(dictionary: response)
                onResponse?(user)
            }
        }
        myRequest.shouldSaveInCache = false
        myRequest.makeRequest()
        return myRequest
    }
}
