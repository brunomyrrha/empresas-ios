//
//  EnterpriseRequest.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 22/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

class EnterpriseRequest : APIRequest {
    
    @discardableResult
    static func getEnterpriseList(onResponse: (([Enterprise]) -> Void)?, onError: ((API.RequestError) -> Void)?) -> EnterpriseRequest {
        let myRequest = EnterpriseRequest(
            method: .get,
            path: "enterprises",
            parameters: nil,
            urlParameters: nil,
            cacheOption: .networkOnly)
        {
            (response,error,cache) in
            if let error = error {
                onError?(error)
            } else if let response = response as? JSONDictionary, let responseContents = response["enterprises"] as? JSONArray {
                let enterpriseListResponse = responseContents.map{
                    Enterprise(dictionary: $0) //$0 all arguments by lambda method.
                }
                onResponse?(enterpriseListResponse)
            }
        }
        myRequest.shouldSaveInCache = false
        myRequest.makeRequest()
        return myRequest
    }
}
