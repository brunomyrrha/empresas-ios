//
//  Portfolio.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 21/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

struct Portfolio : Mappable {
    var enterprisesNumber : Int
    var enterprises : Array<String>
    
    init(mapper : Mapper) {
        self.enterprisesNumber = mapper.keyPath("enterprises_number")
        self.enterprises = mapper.keyPath("enterprises")
    }
}
