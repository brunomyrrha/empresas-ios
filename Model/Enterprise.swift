//
//  EnterpriseModel.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 17/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

struct Enterprise : Mappable{
    var id : Int
    var emailEnterprise : String?
    var facebook : String?
    var twitter : String?
    var linkedin : String?
    var phone : String?
    var ownEnterprise : Bool?
    var enterpriseName : String?
    var photo : String?
    var description : String?
    var city : String?
    var country : String?
    var value : Int?
    var sharePrice : Int?
    var entepriseType : EnterpriseType?
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.emailEnterprise = mapper.keyPath("email_enterprise")
        self.facebook = mapper.keyPath("facebook")
        self.twitter = mapper.keyPath("twitter")
        self.linkedin = mapper.keyPath("linkedin")
        self.phone = mapper.keyPath("phone")
        self.ownEnterprise = mapper.keyPath("own_enterprise")
        self.enterpriseName = mapper.keyPath("enterprise_name")
        self.photo = mapper.keyPath("photo")
        self.description = mapper.keyPath("description")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.value = mapper.keyPath("value")
        self.sharePrice = mapper.keyPath("share_price")
        self.entepriseType = mapper.keyPath("enterprise_type")
    }
}
