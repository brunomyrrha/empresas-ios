//
//  Investor.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 21/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

struct Investor : Mappable {
    var id : Int
    var investorName : String
    var email : String
    var city : String
    var country : String
    var balance : Double
    var photo : String?
    var portfolio : Portfolio
    var portfolioValue : Double
    var firstAccess : Bool
    var superAngel : Bool
    
    init(mapper : Mapper) {
        self.id = mapper.keyPath("id")
        self.investorName = mapper.keyPath("investor_name")
        self.email = mapper.keyPath("email")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.balance = mapper.keyPath("balance")
        self.photo = mapper.keyPath("photo")
        self.portfolio = mapper.keyPath("portfolio")
        self.portfolioValue = mapper.keyPath("portfolio_value")
        self.firstAccess = mapper.keyPath("first_access")
        self.superAngel = mapper.keyPath("super_angel")
    }
}
