//
//  EnterpriseType.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 17/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

struct EnterpriseType : Mappable{
    var id : Int
    var enterpriseTypeName : String
    
    init(mapper: Mapper) {
        self.id = mapper.keyPath("id")
        self.enterpriseTypeName = mapper.keyPath("enterprise_type_name")
    }
}
