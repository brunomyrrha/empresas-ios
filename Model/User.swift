//
//  UserModel.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 17/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

struct User : Mappable {
    //declare variables to hold json data
    var investor : Investor
    var enterprise : String?
    var success : Bool
    
    //init mapper class to make the user dictionary be converted to variables
    init(mapper : Mapper) {
        self.investor = mapper.keyPath("investor")
        self.enterprise = mapper.keyPath("enterprise")
        self.success = mapper.keyPath("success")
    }
}
