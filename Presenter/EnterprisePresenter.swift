//
//  EnterprisePresenter.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 13/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

protocol EnterpriseProtocol : class {
    func showAlert(title : String, message : String)
    func populateList(_ enterpriseList : [Enterprise])
}

class EnterprisePresenter {
    
    var view : EnterpriseViewController?
    
    func attachView(_ view : EnterpriseViewController) {
        self.view = view
    }
    
    func getEnterprises() {
        EnterpriseRequest.getEnterpriseList(
            onResponse: {
                (response) in
                self.view?.populateList(response)
        }, onError: {
                (error) in
                self.view?.showAlert(title: "Erro", message: "Ocorreu um erro ao tentar ler a lista de empresas.\nTente novamente mais tade.", buttonText: "Ok")
        })
    }
}
