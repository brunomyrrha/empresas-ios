//
//  LoginPresenter.swift
//  empresas-ios
//
//  Created by Bruno Diniz on 16/09/18.
//  Copyright © 2018 Bruno Diniz. All rights reserved.
//

import Foundation

protocol LoginProtocol : class {
    func showAlert(title : String, message : String)
    func loginDone(user : User)
}

class LoginPresenter {
    
    var view : LoginViewController?
    var callback : APIRequest.ResponseBlock<User>?
    
    func attachView (_ view : LoginViewController) {
        self.view = view
    }
    
    func makeLogin(login : String, password : String) {
            LoginRequest.makeLogin(username: login, password: password, onResponse:
                { (response) in
                    self.view?.loginDone(user : response)
            }, onError:
                { (error) in
                    self.view?.showAlert(title: "Erro", message: error.failureReason ?? "Erro não identificado.")
        })
    }
}
